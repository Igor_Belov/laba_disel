class CreateSubsections < ActiveRecord::Migration[5.2]
  def change
    create_table :subsections do |t|
      t.string :title
      t.boolean :is_closed, default: false

      t.timestamps
    end
  end
end
