class AddSubsectionToTopics < ActiveRecord::Migration[5.2]
  def change
    add_reference :topics, :subsection, foreign_key: true
  end
end
