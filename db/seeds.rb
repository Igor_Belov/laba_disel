# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = Section.create(
        title: 'Куплю-Продам'
)

user = User.create(
        name: 'Admin',
        email: 'admin@example.com',
        password: 'Passw0rd',
        password_confirmation: 'Passw0rd'
)

user.add_role "admin"

user = User.create(
        name: 'Moderator',
        email: 'moderator@example.com',
        password: 'Passw0rd',
        password_confirmation: 'Passw0rd'
)

user.add_role "moderator", Section.first