Rails.application.routes.draw do
  devise_for :users
  get 'sections/index'
	root 'sections#index'
  ActiveAdmin.routes(self)
  resources :sections
  resources :topics
  resources :comments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
