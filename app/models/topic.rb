class Topic < ApplicationRecord
	resourcify
	belongs_to :subsection
	has_many :comments, dependent: :destroy
	belongs_to :user
end
