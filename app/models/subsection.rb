class Subsection < ApplicationRecord
	resourcify
	belongs_to :section
	has_many :topics, dependent: :destroy
end
