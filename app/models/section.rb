class Section < ApplicationRecord
	resourcify
	has_many :subsections, dependent: :destroy
end
